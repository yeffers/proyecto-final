//
//  ViewController.m
//  proyectofinal
//
//  Created by centro docente de computos on 20/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "Empleados.h"

@interface ViewController (){
    Empleados * consulEmp;
    NSString * cedu;
    NSString * password;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    consulEmp = [[Empleados alloc]init];
    [consulEmp createDatabaseInDocuments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"iniciar"]){
    Empleados *empleados= [Empleados empleado];
    [empleados setEmpName:self.cedula.text];
    empleados.empClave = _contrasena.text;
    
    
    if ([empleados validateUser:empleados.empName andPass:empleados.empClave]) {
        return YES;
    }
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"El usuario no existe en la base de datos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
           [alert show];
    return NO;
    }
    return YES;
}
//- (IBAction)validarU:(id)sender {
//    cedu=_cedula.text;
//    
//    NSUserDefaults * defauls=[NSUserDefaults standardUserDefaults];
//    [defauls setObject:cedu forKey:@"cedul"];
//    
//    consulEmp.empId=_cedula.text;
//    consulEmp.empClave=_contrasena.text;
//    [consulEmp searchEmployeOut];
//    
//    BOOL valid = *(consulEmp.empKey);
//    if (valid) {
//        [_viewAlert setHidden:NO];
//        
//    }else{
//        [_viewAlert setHidden:YES];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"El usuario no existe en la base de datos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
//}

- (IBAction)salir:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registrar:(id)sender {
}

- (IBAction)ingresar:(id)sender {
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UIView *view in self.view.subviews) {
        [view resignFirstResponder];
    }
}
@end
