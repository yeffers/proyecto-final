//
//  Empleados.h
//  proyectofinal
//
//  Created by cassey on 5/21/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Empleados : NSObject{
    sqlite3 * empleadosdb;
}


@property (strong, nonatomic) NSString * empId;
@property (strong, nonatomic) NSString * empName;
@property (strong, nonatomic) NSString * empAge;
@property (strong, nonatomic) NSString * empCedula;
@property (strong, nonatomic) NSString * empEmail;
@property (strong, nonatomic) NSString * empClave;
@property (strong, nonatomic) NSString * status;
//@property BOOL * empKey;

+(id)empleado;

-(void)createDatabaseInDocuments;
-(void)searchEmployedInDataBasebyId;
-(void)createEmployedInDataBase;
-(void)searchEmployeOut;
-(BOOL)validateUser:(NSString *)user andPass:(NSString *)pwd;

@end
