//
//  listaputasViewController.m
//  proyectofinal
//
//  Created by cassey on 5/26/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "listaputasViewController.h"
#import "servisocial.h"
#import "detallelistaViewController.h"

static NSString *cellIdentifier = @"cell";
@interface listaputasViewController ()

@end

@implementation listaputasViewController{
    servisocial *fotomulta;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Listado de mujeres";
    servisocial *servi=[[servisocial alloc]init];
    _fotomultas = [servi fotomultasPlaca:_user];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    detallelistaViewController *detalleVC = [segue destinationViewController];
    detalleVC.fotomulta = fotomulta;
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _fotomultas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [listaTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    fotomulta = _fotomultas[indexPath.row];
    
    cell.textLabel.text = fotomulta.nombre;
    cell.detailTextLabel.text = fotomulta.descripcion;
    cell.imageView.image = [UIImage imageNamed:fotomulta.nombreImagen];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    fotomulta = _fotomultas[indexPath.row];
    
    [self performSegueWithIdentifier:@"detalle" sender:self];
}

- (IBAction)logout:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
