//
//  registroViewController.m
//  proyectofinal
//
//  Created by cassey on 5/21/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "registroViewController.h"
#import "Empleados.h"

@interface registroViewController ()

@end

@implementation registroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    nuevoEmpleado = [[Empleados alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UIView * view in self.view.subviews) {
        [view resignFirstResponder];
    }
}


- (IBAction)saveEmp:(id)sender {
    if ( [_empNameNew.text length] == 0 || [_empClaveNew.text length] == 0 ) {
        UIAlertView *mensa = [[UIAlertView alloc]initWithTitle:@"Registro" message:@"usuario no se a registrado" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [mensa show];
        
    }else {
        int k = [_empAgeNew.text intValue];
        if (k < 18) {
            UIAlertView *mensa = [[UIAlertView alloc]initWithTitle:@"Registro" message:@"usuario menor de edad lo sentimos bebe" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [mensa show];
        } else {
            nuevoEmpleado.empName = _empNameNew.text;
            nuevoEmpleado.empCedula = _empCedulaNew.text;
            nuevoEmpleado.empAge = _empAgeNew.text;
            nuevoEmpleado.empEmail = _empEmailNew.text;
            nuevoEmpleado.empClave = _empClaveNew.text;
            
            [nuevoEmpleado createEmployedInDataBase];
            _statustext.text = nuevoEmpleado.status;
            
            UIAlertView *mensa = [[UIAlertView alloc]initWithTitle:@"Registro" message:@"usuario registrado" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [mensa show];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
  
    }
}
@end
