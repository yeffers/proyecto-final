//
//  ViewController.h
//  proyectofinal
//
//  Created by centro docente de computos on 20/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *cedula;
@property (strong, nonatomic) IBOutlet UITextField *contrasena;



- (IBAction)salir:(id)sender;

- (IBAction)registrar:(id)sender;
- (IBAction)ingresar:(id)sender;

@end

