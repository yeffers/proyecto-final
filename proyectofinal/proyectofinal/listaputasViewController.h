//
//  listaputasViewController.h
//  proyectofinal
//
//  Created by cassey on 5/26/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface listaputasViewController : UIViewController{
    
    __weak IBOutlet UITableView *listaTableView;
}

@property (strong, nonatomic) NSString *user;

@property(strong, nonatomic)NSArray *fotomultas;

- (IBAction)logout:(id)sender;

@end
