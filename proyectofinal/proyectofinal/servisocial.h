//
//  servisocial.h
//  proyectofinal
//
//  Created by cassey on 5/26/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface servisocial : NSObject

@property (strong, nonatomic) NSString *nombre;
@property (strong, nonatomic) NSString *descripcion;
@property (strong, nonatomic) NSString *nombreImagen;
@property (strong, nonatomic) NSString *telefono;


- (instancetype)initWithFecha:(NSString *)nombre descripcion:(NSString *)descripcion telefono:(NSString *)telefono nombreImagen:(NSString *)nombreImagen;

- (NSArray *)fotomultasPlaca:(NSString *)placa;

@end
