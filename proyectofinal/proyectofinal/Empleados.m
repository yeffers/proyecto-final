//
//  Empleados.m
//  proyectofinal
//
//  Created by cassey on 5/21/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "Empleados.h"

@interface Empleados()

@property(nonatomic, strong) NSString * databasePath;

@end

@implementation Empleados


+(id)empleado{
    Empleados * instance= [[Empleados alloc]init];
    return instance;
}



-(void)searchPathDatabase{
    NSString * rutaDoc= [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    _databasePath = [[NSString alloc]initWithString:[rutaDoc stringByAppendingPathComponent:@"emple.db"]];
}

-(void)createDatabaseInDocuments{
    
    [self searchPathDatabase];
    
    NSFileManager *fm= [[NSFileManager alloc] init];
    //buscar la ruta y guardarla
    NSLog(@"%@", _databasePath);
    
    //buscar el archivo, lo demas es c puro
    
    if ([fm fileExistsAtPath:_databasePath]==NO) {
        NSLog(@"El archivo no existe");
        //C puro se convierte en UTF8string como si fuera un array de caracteres
        const char * dbpath=[_databasePath UTF8String];
        //abrir mi base de datos, con parametros , ruta base de datos y la direccion de memoria de la variable sqlite3, y ocn esto retorna si el archivo no esta lo crea
        if (sqlite3_open(dbpath, &empleadosdb)==SQLITE_OK) {
            NSLog(@"El archivo fue creado");
            //va manejar el error
            char * errMsg;
            //my query, creando la tabla sino existe
            const char * sql_stmt= "CREATE TABLE IF NOT EXISTS EMPL(ID INTEGER PRIMARY KEY AUTOINCREMENT, EMP_NAME TEXT,EMP_CEDULA TEXT, EMP_AGE TEXT, EMP_EMAIL TEXT, EMP_CLAVE TEXT)";
            //envio mi query sqlite3, SQLITE_OK  es una constante
            if (sqlite3_exec(empleadosdb, sql_stmt, NULL, NULL, &errMsg)==  SQLITE_OK) {
                NSLog(@"Tabla Creada Exitosamente!!..");
            }else{
                NSLog(@"Error en crear Tabla!!..:%s", errMsg);
            }
        }else{
            NSLog(@"Error en crear la base de datos");
        }
    }else{
        NSLog(@"El archivo ya existe, no se remplazo");
    }
    
}

-(BOOL)validateUser:(NSString *)user andPass:(NSString *)pwd{
    [self searchPathDatabase];
    const char*db = [_databasePath UTF8String];
    //hacer el query
    sqlite3_stmt * query;
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK)
    {
        NSString * select = [NSString stringWithFormat:@"SELECT * FROM EMPL WHERE EMP_NAME = \"%@\"", _empName];
        
        //convertir
        const char * select_sql = [select UTF8String];
        //-1 lo que mas oueda retornar
        if (sqlite3_prepare_v2(empleadosdb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            //recorre un solo empleado por ello se utiliza un if
            if (sqlite3_step(query)==SQLITE_ROW) {
                _status=@"Registro encontrado";
                //con @"%s" para pasar de c a objetive c
                _empName= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 1)];
                _empCedula= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 2)];
                _empAge= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 3)];
                _empEmail= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 4)];
                _empClave=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 5)];
                
            }else{
                _status=@"Registro no encontrado";
                return  NO;
            }
            sqlite3_finalize(query);
        }else{
            _status=@"Error en el query";
            return NO;
        }
        //finalizo el query y cierro la base de datos
        
        sqlite3_close(empleadosdb);
    }else{
        _status =@"No se puede abrir la base de datos";
        return NO;
    }
    if ([self.empName isEqualToString:_empName] && [pwd isEqualToString:_empClave]){
        return YES;
    }
    return NO;
    
}

-(void)createEmployedInDataBase{
    [self searchPathDatabase];
    sqlite3_stmt * query;
    const char * db= [_databasePath UTF8String];
    //& direccion de memoria
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK) {
        NSString * insert = [NSString stringWithFormat:@"INSERT INTO EMPL (EMP_NAME,EMP_CEDULA, EMP_AGE, EMP_EMAIL, EMP_CLAVE) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",_empName,_empCedula,_empAge, _empEmail, _empClave];
        
        const char * insert_sql = [insert UTF8String];
        sqlite3_prepare_v2(empleadosdb, insert_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            _status = @"Registro Almacenado";
        }else{
            _status = @"Registro No Almacenado";
        }
        sqlite3_finalize(query);
        sqlite3_close(empleadosdb);
    }else{
        _status= @"No se pudo abrir la base de datos";
    }
}


-(void)searchEmployedInDataBasebyId{
    [self searchPathDatabase];
    const char*db = [_databasePath UTF8String];
    //hacer el query
    sqlite3_stmt * query;
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK)
    {
        NSString * select = [NSString stringWithFormat:@"SELECT * FROM EMPL WHERE ID = \"%@\"", _empId];
        
        //convertir
        const char * select_sql = [select UTF8String];
        //-1 lo que mas oueda retornar
        if (sqlite3_prepare_v2(empleadosdb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            //recorre un solo empleado por ello se utiliza un if
            if (sqlite3_step(query)==SQLITE_ROW) {
                _status=@"Registro encontrado";
                //con @"%s" para pasar de c a objetive c
                _empId= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 0)];
                _empName= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 1)];
                _empAge= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 2)];
                _empEmail= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 3)];
                
                _empClave=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 4)];
    
            }else{
                _status=@"Registro no encontrado";
            }
            sqlite3_finalize(query);
        }else{
            _status=@"Error en el query";
        }
        //finalizo el query y cierro la base de datos
        
        sqlite3_close(empleadosdb);
    }else{
        _status =@"No se puede abrir la base de datos";
    }
    
}













@end
