//
//  registroViewController.h
//  proyectofinal
//
//  Created by cassey on 5/21/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "Empleados.h"

@interface registroViewController : ViewController
{
    Empleados *nuevoEmpleado;

}
@property (strong, nonatomic) IBOutlet UITextField *empNameNew;
@property (strong, nonatomic) IBOutlet UITextField *empCedulaNew;
@property (strong, nonatomic) IBOutlet UITextField *empAgeNew;
@property (strong, nonatomic) IBOutlet UITextField *empEmailNew;
@property (strong, nonatomic) IBOutlet UITextField *empClaveNew;


@property (strong, nonatomic) IBOutlet UILabel *statustext;

- (IBAction)saveEmp:(id)sender;

@end
