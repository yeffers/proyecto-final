//
//  detallelistaViewController.h
//  proyectofinal
//
//  Created by cassey on 5/26/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "servisocial.h"

@interface detallelistaViewController : UIViewController{
    
    __weak IBOutlet UIImageView *foto;
    __weak IBOutlet UILabel *fechaLabel;
    __weak IBOutlet UILabel *horaLabel;
    __weak IBOutlet UILabel *infraccionLabel;
    
}

@property(strong, nonatomic) servisocial *fotomulta;
- (IBAction)botton:(id)sender;

@end
